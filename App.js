import './App.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';



function fetchAPI(number) {
  return fetch("https://r3tcwj4jwl.execute-api.us-east-2.amazonaws.com/default/lambdaFunction?n=" + number);
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
		    <h1>How many values to generate ? (0-9)</h1>
		    <input id="number" type='number' />
		      <div><Button>Generate</Button></div>
      </header>
    </div>
  );
}

class Button extends React.Component {
  constructor(){
	  super();
	  this.state = { result: null };
  }

  toggleButtonState = () => {
  let chosenNumber = parseInt(document.getElementById("number").value);
	console.log(chosenNumber)
  fetchAPI(chosenNumber).then(result => {result.text().then(text => {this.setState({result: text.replaceAll("," , "  ").replace('[', '').replace(']', '')})})})
  };


  render() {
    return (
      <div>
        <button onClick={this.toggleButtonState}>Generate</button>
		    <p>result:</p>
        <div>{this.state.result}</div>
      </div>
    );
  }
}

export default App;
